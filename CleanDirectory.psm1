﻿<#
.SYNOPSIS
Remove files older than a defined number of days from a given directory.

.DESCRIPTION
The Clean-Directory function removes files older than a defined number of days from a given directory

.PARAMETER directory
Directory which is to be cleaned.

.PARAMETER interval 
set the time unit (months,days,hours,minutes)

.PARAMETER count
set the amount of time units

.EXAMPLE
Remove files older than 3 days from D:\temp
CleanDirectory -directory "D:\temp" -interval days -count 3

.NOTES
TODO: check if correct directory is cleaned?
#>

function CleanDirectory {
    [CmdletBinding(SupportsShouldProcess=$true, ConfirmImpact="high")]
    param (
            [parameter(Mandatory=$true)]
            [string[]]$directory="D:\temp",
            
            [parameter(Mandatory=$true)]
            [string[]]$interval="days",

            [parameter(Mandatory=$true)]
            [string]$count=3

        )
        
        PROCESS {
            
            $disk = ($directory -split ":",2)[0]
            # check if D: disk
            if ($disk -eq "C") {
                write-host "Geen C: schijf"
                break
            }

            $limit=""
        
            if ($interval -eq "months") {
                $limit = (Get-Date).AddMonths(-$count)
            }

            if ($interval -eq "days") {
	            $limit = (Get-Date).AddDays(-$count)
            }
        
            if ($interval -eq "hours") {
                $limit = (Get-Date).AddHours(-$count)
            }
        
            if ($interval -eq "minutes") {
                $limit = (Get-Date).AddMinutes(-$count)
            }
        
            if ($limit -and (Test-Path -path $directory -IsValid)) {
                write-host "Cleaning files and directories in $directory older than $limit"
                Get-ChildItem -Path $directory -Recurse -Force | Where-Object { !$_.PSIsContainer -and $_.CreationTime -lt $limit } | Remove-Item -Force -Verbose
                Get-ChildItem -Path $directory -Recurse -Force | Where-Object { $_.PSIsContainer -and (Get-ChildItem -Path $_.FullName -Recurse -Force | Where-Object { !$_.PSIsContainer }) -eq $null } | Remove-Item -Force -Recurse -Verbose
               
            }
            
            
        }
}
export-modulemember -function CleanDirectory